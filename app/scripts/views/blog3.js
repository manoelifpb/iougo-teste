/*global iougoTeste, Backbone, JST*/

iougoTeste.Views = iougoTeste.Views || {};

(function () {
    'use strict';

    iougoTeste.Views.Blog3View = Backbone.View.extend({

        template: JST['app/scripts/templates/blog3.ejs']

    });

})();
