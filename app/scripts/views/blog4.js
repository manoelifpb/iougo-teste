/*global iougoTeste, Backbone, JST*/

iougoTeste.Views = iougoTeste.Views || {};

(function () {
    'use strict';

    iougoTeste.Views.Blog4View = Backbone.View.extend({

        template: JST['app/scripts/templates/blog4.ejs']

    });

})();
