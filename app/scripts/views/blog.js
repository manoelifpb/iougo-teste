/*global iougoTeste, Backbone, JST*/

iougoTeste.Views = iougoTeste.Views || {};

(function () {
    'use strict';

    iougoTeste.Views.BlogView = Backbone.View.extend({

        template: JST['app/scripts/templates/blog.ejs'],

        render : function() {
            this.$el.html( this.template(this.model.toJSON()) );
            return this;
        }

    });

})();
